#define _DEFAULT_SOURCE

#include <assert.h>
#include <unistd.h>

#include "mem.h"
#include "test.h"
#include "util.h"


void debug(const char *fmt, ...);

static const size_t INITIAL_HEAP_SIZE = 10000;
static const size_t TEST_ADDR_LENGTH = 1000;
static const size_t TEST_BLOCK_SIZE = 1000;
static const size_t TEST3_EXPECTED_SIZE = 2000;
static const size_t TEST4_1_BLOCK_SIZE = 10000;
static const size_t TEST4_2_BLOCK_SIZE = 5000;
static const size_t TEST5_1_BLOCK_SIZE = 10000;
static const size_t TEST5_2_BLOCK_SIZE = 100000;

static void *init_heap() {
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    assert(heap != NULL && "heap not initialized");
    return heap;
}

static void create_heap(struct block_header *last_block) {
    struct block_header *addr = last_block;
    void *test_addr = (uint8_t *) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = mmap(test_addr, TEST_ADDR_LENGTH, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    debug(test_addr);
}

static struct block_header *block_frm_cont(void *data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

static inline void test1(struct block_header *block) {
    void *data = _malloc(TEST_BLOCK_SIZE);
    assert(data != NULL && "_malloc returns NULL");
    debug_heap(stdout, block);
    assert(!block->is_free && "is_free must be false");
    assert(block->capacity.bytes == TEST_BLOCK_SIZE && "capacity must be 1000 bytes");
    _free(data);
}

static inline void test2(struct block_header *block) {
    void *data1 = _malloc(TEST_BLOCK_SIZE);
    void *data2 = _malloc(TEST_BLOCK_SIZE);
    assert(data1 != NULL && data2 != NULL && "_malloc returns NULL");

    _free(data1);
    debug_heap(stdout, block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data2_block = block_frm_cont(data2);
    assert(data1_block->is_free && "is_free must be true");
    assert(!data2_block->is_free && "is_free must be false");
    _free(data2);
}

static inline void test3(struct block_header *block) {
    void *data1 = _malloc(TEST_BLOCK_SIZE);
    void *data2 = _malloc(TEST_BLOCK_SIZE);
    void *data3 = _malloc(TEST_BLOCK_SIZE);
    assert(data1 != NULL && data2 != NULL && data3 != NULL && "_malloc returns NULL");

    _free(data2);
    _free(data1);
    debug_heap(stdout, block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data3_block = block_frm_cont(data3);
    assert(data1_block->is_free && "is_free must be true");
    assert(!data3_block->is_free && "is_free must be false");
    assert(data1_block->capacity.bytes == TEST3_EXPECTED_SIZE + offsetof(struct block_header, contents) && "unexpected block size");
    _free(data3);
}

static inline void test4(struct block_header *block) {
    void *data1 = _malloc(TEST4_1_BLOCK_SIZE);
    void *data2 = _malloc(TEST4_1_BLOCK_SIZE);
    void *data3 = _malloc(TEST4_2_BLOCK_SIZE);
    assert(data1 != NULL && data2 != NULL && data3 != NULL && "_malloc returns NULL");

    _free(data2);
    _free(data3);
    debug_heap(stdout, block);
    struct block_header *data1_block = block_frm_cont(data1);
    struct block_header *data2_block = block_frm_cont(data2);
    assert((uint8_t *) data1_block->contents + data1_block->capacity.bytes == (uint8_t *) data2_block && "block must be created near first created block");
    _free(data1);
}

static inline void test5(struct block_header *block) {
    void *data1 = _malloc(TEST5_1_BLOCK_SIZE);
    assert(data1 != NULL && "_malloc returns NULL");
    struct block_header *addr = block;
    while (addr->next != NULL) {
        addr = addr->next;
    }
    create_heap(addr);

    void *data2 = _malloc(TEST5_2_BLOCK_SIZE);
    debug_heap(stdout, block);
    struct block_header *data2_block = block_frm_cont(data2);
    assert(data2_block != addr && "block must be created in another location");
    _free(data1);
    _free(data2);
}

void test() {
    struct block_header *block = (struct block_header *) init_heap();
    test1(block);
    test2(block);
    test3(block);
    test4(block);
    test5(block);
    _free(block);
    debug("\nAll tests passed");
}